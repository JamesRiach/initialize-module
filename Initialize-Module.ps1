# Windows PowerShell Module Helper
# Import a Module Ready for use, optionally Install the module if not already available or
# Check for Updates to Module prior to importing


Function Initialize-Module
{
    [CmdletBinding()]

    param (

    [Parameter(Mandatory=$true)]
    [string]$Module,                            # The name of the Module

    [string]$Repository = "PSGallery",          # Specify Repository to install module from, default is PSGallery
    [switch]$InstallModule,                     # Specify if the Module should be installed if it isn't already
    [switch]$UpdateModule                       # Specify if to check for an update for the Module

    ) #end param

    # Is the module imported
    $moduleImported = Get-Module -Name $Module;
    if ($null -eq $moduleImported){
        Write-Verbose "$Module not currently imported";

        # If not imported, is it available
        $moduleAvailable = Get-Module -ListAvailable -Name $Module -Verbose:$VerbosePreference;

        if ($null -ne $moduleAvailable){
            # Check for Module Updates?
            if ($UpdateModule){
                Update-Module -Name $Module -Scope AllUsers -Verbose:$VerbosePreference;
            }

            # Import the module
            Write-Verbose "Importing Module: $Module";
            Import-Module -Name $Module -Verbose:$VerbosePreference;
        }
        elseif (($null -eq $moduleAvailable) -and ($InstallModule -eq $true)){
            # Install the module
            Write-Verbose "Installing Module: $Module";
            Set-PSRepository -Name $Repository -InstallationPolicy Trusted;

            Install-Module -Name $Module -Repository $Repository -Scope AllUsers -Force -Verbose:$VerbosePreference;

            # Re-call the function to check module is now available and import it, but set InstallModule to False so as to prevent an infinite loop
            Initialize-Module -Module $Module -InstallModule:$false;
        }
        else {
            # If the module is not imported, available and not to be installed, exit with failure
            Throw "$Module is not installed or available and will not be installed";

            Exit 1;
        }
    }
} #end function